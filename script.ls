$ !->
	music = new Howl {
		src: ['audio/bensound-scifi.mp3'],
		loop: true,
		autoplay: true,
		volume: 0.3,
		rate: 1.5
	}

	fps = 30
	interval = 1000/fps

	f = new numberformat.Formatter { format: 'standard', sigfigs: 5, flavor: 'short' }

	tutorialStage = 1
	inTutorial = true

	processes = 100
	energy = robots = quantum = 0

	processStructs =
		transistors: 0
		microprocessors: 0
		macroprocessors: 0
		harddrives: 0
	# energyStructures, etc. not here yet

	processCosts =
		transistors: 10
		microprocessors: 1000
		macroprocessors: 1000_000 # 1 million
		harddrives: 1_000_000_000 # 1 billion
	
	const PROCESS_LIST =
		\transistors
		\microprocessors
		\macroprocessors
		\harddrives

	increase-values = !->
		processes += processStructs.transistors / fps
		PROCESS_LIST.forEach (name, index) !->
			if index == 0 then return
			prev = PROCESS_LIST[index - 1]
			processStructs[prev] += processStructs[name] * (index + 1) / fps
	
	render-structs = !->
		PROCESS_LIST.forEach (name) !->
			$(".#{name}-display").text f.format(processStructs[name])
	
	update = !->
		increase-values()
		render-structs()

		$('#processes-display').text f.format(processes)
		$('#energy-display').text f.format(energy)
		$('#robots-display').text f.format(robots)
		$('#quantum-display').text f.format(quantum)

		if tutorialStage == 1 and processStructs.transistors >= 10
			tutorialStage++
			$('#tutorial-1').remove()
		else if tutorialStage == 2 and processes >= 1000 and processStructs.transistors >= 100
			tutorialStage++
			$('#tutorial-2').remove()
		else if tutorialStage == 3 and processStructs.microprocessors >= 1
			tutorialStage++
			$('#tutorial-3').remove()
		
		
		if tutorialStage <= 4 and inTutorial
			$('#tutorial-' + tutorialStage).css('display', 'block')
			if tutorialStage == 4 then inTutorial := false

	repeater = setInterval update, interval
	
	PROCESS_LIST.forEach (name, index) !->
		$('#buy-' + name).on 'click', !->
			cost = processCosts[name]
			
			if name == 'transistors' and cost <= processes
				processStructs[name]++
				processes -= cost
			else if name != 'transistors' and cost <= processes and processStructs[PROCESS_LIST[index-1]] >= Math.pow(10, index+1)
				processStructs[name]++
				processes -= cost
				processStructs[PROCESS_LIST[index-1]] -= Math.pow(10, index+1)
	
	PROCESS_LIST.forEach (name, index) !->
		$('#buy-max-' + name).on 'click', ->
			cost = processCosts[name]
			total = processes - (processes % cost)
			if total == 0 then return
			amount = total / cost
			
			if name == 'transistors' && cost <= processes
				processStructs[name] += amount
				processes -= total
			else if name != 'transistors' && cost <= processes && processStructs[PROCESS_LIST[index-1]] >= Math.pow(10, index+1)
				processStructs[name] += amount
				processes -= total
				processStructs[PROCESS_LIST[index-1]] -= amount*Math.pow(10, index+1)

	update-abbr = !->
		PROCESS_LIST.forEach (name) !->
			$("\##{name}-cost").text f.format(processCosts[name])
	update-abbr()

	$ '#theme-chooser' .on 'change', !->
		$ '#theme' .remove()

		src = if $ this .val() != 'bootstrap'
			then 'https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/'+$ this .val()+'/bootstrap.min.css'
			else 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'

		$ 'head' .append '<link rel="stylesheet" type="text/css" href="'+src+'" id="theme">'
	
	$ '#abbr-chooser' .on 'change', !->
		opts = JSON.parse $(this).val()
		opts.sigfigs = 5

		f := new numberformat.Formatter opts

		update-abbr()
	
	$ '#delagify-chooser' .on 'click', !->
		clear-interval repeater
		fps := parseInt $(this).val()
		interval := 1000/fps
		repeater := setInterval update, interval
	
	$ '#toggle-music' .on 'click', !->
		$ '#music-on' .text(if this.checked then 'On' else 'Off')
		if this.checked then music.fade(0, 0.3, 500) else music.fade(0.3, 0, 500)
	# END <anonymous>