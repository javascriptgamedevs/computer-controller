// Generated by LiveScript 1.5.0
(function(){
  $(function(){
    var music, fps, interval, f, tutorialStage, inTutorial, processes, energy, robots, quantum, processStructs, processCosts, PROCESS_LIST, increaseValues, renderStructs, update, repeater, updateAbbr;
    music = new Howl({
      src: ['audio/bensound-scifi.mp3'],
      loop: true,
      autoplay: true,
      volume: 0.3,
      rate: 1.5
    });
    fps = 30;
    interval = 1000 / fps;
    f = new numberformat.Formatter({
      format: 'standard',
      sigfigs: 5,
      flavor: 'short'
    });
    tutorialStage = 1;
    inTutorial = true;
    processes = 100;
    energy = robots = quantum = 0;
    processStructs = {
      transistors: 0,
      microprocessors: 0,
      macroprocessors: 0,
      harddrives: 0
    };
    processCosts = {
      transistors: 10,
      microprocessors: 1000,
      macroprocessors: 1000000,
      harddrives: 1000000000
    };
    PROCESS_LIST = ['transistors', 'microprocessors', 'macroprocessors', 'harddrives'];
    increaseValues = function(){
      processes += processStructs.transistors / fps;
      PROCESS_LIST.forEach(function(name, index){
        var prev;
        if (index === 0) {
          return;
        }
        prev = PROCESS_LIST[index - 1];
        processStructs[prev] += processStructs[name] * (index + 1) / fps;
      });
    };
    renderStructs = function(){
      PROCESS_LIST.forEach(function(name){
        $("." + name + "-display").text(f.format(processStructs[name]));
      });
    };
    update = function(){
      increaseValues();
      renderStructs();
      $('#processes-display').text(f.format(processes));
      $('#energy-display').text(f.format(energy));
      $('#robots-display').text(f.format(robots));
      $('#quantum-display').text(f.format(quantum));
      if (tutorialStage === 1 && processStructs.transistors >= 10) {
        tutorialStage++;
        $('#tutorial-1').remove();
      } else if (tutorialStage === 2 && processes >= 1000 && processStructs.transistors >= 100) {
        tutorialStage++;
        $('#tutorial-2').remove();
      } else if (tutorialStage === 3 && processStructs.microprocessors >= 1) {
        tutorialStage++;
        $('#tutorial-3').remove();
      }
      if (tutorialStage <= 4 && inTutorial) {
        $('#tutorial-' + tutorialStage).css('display', 'block');
        if (tutorialStage === 4) {
          inTutorial = false;
        }
      }
    };
    repeater = setInterval(update, interval);
    PROCESS_LIST.forEach(function(name, index){
      $('#buy-' + name).on('click', function(){
        var cost;
        cost = processCosts[name];
        if (name === 'transistors' && cost <= processes) {
          processStructs[name]++;
          processes -= cost;
        } else if (name !== 'transistors' && cost <= processes && processStructs[PROCESS_LIST[index - 1]] >= Math.pow(10, index + 1)) {
          processStructs[name]++;
          processes -= cost;
          processStructs[PROCESS_LIST[index - 1]] -= Math.pow(10, index + 1);
        }
      });
    });
    PROCESS_LIST.forEach(function(name, index){
      $('#buy-max-' + name).on('click', function(){
        var cost, total, amount;
        cost = processCosts[name];
        total = processes - processes % cost;
        if (total === 0) {
          return;
        }
        amount = total / cost;
        if (name === 'transistors' && cost <= processes) {
          processStructs[name] += amount;
          return processes -= total;
        } else if (name !== 'transistors' && cost <= processes && processStructs[PROCESS_LIST[index - 1]] >= Math.pow(10, index + 1)) {
          processStructs[name] += amount;
          processes -= total;
          return processStructs[PROCESS_LIST[index - 1]] -= amount * Math.pow(10, index + 1);
        }
      });
    });
    updateAbbr = function(){
      PROCESS_LIST.forEach(function(name){
        $("#" + name + "-cost").text(f.format(processCosts[name]));
      });
    };
    updateAbbr();
    $('#theme-chooser').on('change', function(){
      var src;
      $('#theme').remove();
      src = $(this).val() !== 'bootstrap' ? 'https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/' + $(this).val() + '/bootstrap.min.css' : 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css';
      $('head').append('<link rel="stylesheet" type="text/css" href="' + src + '" id="theme">');
    });
    $('#abbr-chooser').on('change', function(){
      var opts;
      opts = JSON.parse($(this).val());
      opts.sigfigs = 5;
      f = new numberformat.Formatter(opts);
      updateAbbr();
    });
    $('#delagify-chooser').on('click', function(){
      clearInterval(repeater);
      fps = parseInt($(this).val());
      interval = 1000 / fps;
      repeater = setInterval(update, interval);
    });
    $('#toggle-music').on('click', function(){
      $('#music-on').text(this.checked ? 'On' : 'Off');
      if (this.checked) {
        music.fade(0, 0.3, 500);
      } else {
        music.fade(0.3, 0, 500);
      }
    });
  });
}).call(this);
